package persistence

import javax.persistence.*

@Entity
@Table(name = "person")

data class Person(
        @OrderBy("firstName ASC")

        @Id @GeneratedValue
        var id: Int = 0,
        @Column(nullable = false)
        var firstName: String,
        @Column(nullable = false)
        var lastName: String,

        @Embedded
        var something: Something
){

    @OneToMany(mappedBy="valueId.person",
            fetch=FetchType.LAZY)
    lateinit var listOfValues: Collection<Value>

}