package persistence

import javax.persistence.*

@Entity
@Table(name = "value")
data class Value(

        @EmbeddedId
        var valueId: ValueId,
        var selectOP: SelectEnum
)