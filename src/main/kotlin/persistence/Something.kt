package persistence

import javax.persistence.Embeddable

@Embeddable
class Something(
        var something: String?,
        var other: String?
)