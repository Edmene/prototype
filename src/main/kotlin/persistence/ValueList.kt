package persistence

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "valueList")
data class ValueList(
        @Id @GeneratedValue
        var id: Int = 0,
        var date: Date
){

    @OneToMany(mappedBy="valueId.valueList",
            fetch=FetchType.LAZY)
    lateinit var listOfValues: Collection<Value>


}