package util

import java.sql.SQLException
import java.sql.Types

//import org.hibernate.JDBCException
import org.hibernate.ScrollMode

import org.hibernate.dialect.function.AbstractAnsiTrimEmulationFunction
import org.hibernate.dialect.function.NoArgSQLFunction
import org.hibernate.dialect.function.SQLFunction
import org.hibernate.dialect.identity.IdentityColumnSupport
import util.identity.SQLiteDialectIdentityColumnSupport
import org.hibernate.dialect.pagination.AbstractLimitHandler
import org.hibernate.dialect.pagination.LimitHandler
import org.hibernate.dialect.pagination.LimitHelper
import org.hibernate.dialect.unique.DefaultUniqueDelegate
import org.hibernate.dialect.unique.UniqueDelegate

import org.hibernate.engine.spi.RowSelection
import org.hibernate.exception.DataException
import org.hibernate.exception.JDBCConnectionException
import org.hibernate.exception.LockAcquisitionException
import org.hibernate.exception.spi.SQLExceptionConversionDelegate
import org.hibernate.exception.spi.TemplatedViolatedConstraintNameExtracter
import org.hibernate.exception.spi.ViolatedConstraintNameExtracter
import org.hibernate.internal.util.JdbcExceptionHelper
import org.hibernate.mapping.Column
import org.hibernate.type.StandardBasicTypes
import org.hibernate.dialect.Dialect
import org.hibernate.dialect.function.SQLFunctionTemplate
import org.hibernate.dialect.function.StandardSQLFunction
import org.hibernate.dialect.function.VarArgsSQLFunction


class SQLiteDialect : Dialect(){

    private val uniqueDelegate:DefaultUniqueDelegate

    init {

        registerColumnType(Types.BIT, "boolean")
        //registerColumnType(Types.FLOAT, "float");
        //registerColumnType(Types.DOUBLE, "double");
        registerColumnType(Types.DECIMAL, "decimal")
        registerColumnType(Types.CHAR, "char")
        registerColumnType(Types.LONGVARCHAR, "longvarchar")
        registerColumnType(Types.TIMESTAMP, "datetime")
        registerColumnType(Types.BINARY, "blob")
        registerColumnType(Types.VARBINARY, "blob")
        registerColumnType(Types.LONGVARBINARY, "blob")

        registerFunction("concat", VarArgsSQLFunction(StandardBasicTypes.STRING, "", "||", ""))
        registerFunction("mod", SQLFunctionTemplate(StandardBasicTypes.INTEGER, "?1 % ?2"))
        registerFunction("quote", StandardSQLFunction("quote", StandardBasicTypes.STRING))
        registerFunction("random", NoArgSQLFunction("random", StandardBasicTypes.INTEGER))
        registerFunction("round", StandardSQLFunction("round"))
        registerFunction("substr", StandardSQLFunction("substr", StandardBasicTypes.STRING))

        registerFunction("concat", VarArgsSQLFunction(StandardBasicTypes.STRING, "", "||", ""))
        registerFunction("mod", SQLFunctionTemplate(StandardBasicTypes.INTEGER, "?1 % ?2"))
        registerFunction("quote", StandardSQLFunction("quote", StandardBasicTypes.STRING))
        registerFunction("random", NoArgSQLFunction("random", StandardBasicTypes.INTEGER))
        registerFunction("round", StandardSQLFunction("round"))
        registerFunction("substr", StandardSQLFunction("substr", StandardBasicTypes.STRING))
        registerFunction("trim", object : AbstractAnsiTrimEmulationFunction() {
            override fun resolveBothSpaceTrimFunction(): SQLFunction {
                return SQLFunctionTemplate(StandardBasicTypes.STRING, "trim(?1)")
            }

            override fun resolveBothSpaceTrimFromFunction(): SQLFunction {
                return SQLFunctionTemplate(StandardBasicTypes.STRING, "trim(?2)")
            }

            override fun resolveLeadingSpaceTrimFunction(): SQLFunction {
                return SQLFunctionTemplate(StandardBasicTypes.STRING, "ltrim(?1)")
            }

            override fun resolveTrailingSpaceTrimFunction(): SQLFunction {
                return SQLFunctionTemplate(StandardBasicTypes.STRING, "rtrim(?1)")
            }

            override fun resolveBothTrimFunction(): SQLFunction {
                return SQLFunctionTemplate(StandardBasicTypes.STRING, "trim(?1, ?2)")
            }

            override fun resolveLeadingTrimFunction(): SQLFunction {
                return SQLFunctionTemplate(StandardBasicTypes.STRING, "ltrim(?1, ?2)")
            }

            override fun resolveTrailingTrimFunction(): SQLFunction {
                return SQLFunctionTemplate(StandardBasicTypes.STRING, "rtrim(?1, ?2)")
            }
        })

        uniqueDelegate = SQLiteUniqueDelegate(this)
    }

    private val IDENTITY_COLUMN_SUPPORT = SQLiteDialectIdentityColumnSupport()

    override fun getIdentityColumnSupport(): IdentityColumnSupport {
        return IDENTITY_COLUMN_SUPPORT
    }

    object LIMIT_HANDLER : AbstractLimitHandler(){
        override fun processSql(sql: String?, selection: RowSelection?): String {
            val hasOffset = LimitHelper.hasFirstRow(selection)
            return sql + if (hasOffset) " limit ? offset ?" else " limit ?"
        }

        override fun supportsLimitOffset(): Boolean {
            return true
        }

        override fun bindLimitParametersInReverseOrder(): Boolean {
            return true
        }
    }

    override fun getLimitHandler(): LimitHandler {
        return LIMIT_HANDLER
    }

    override fun supportsLockTimeouts(): Boolean {
        return false
    }

    override fun getForUpdateString(): String {
        return ""
    }

    override fun supportsOuterJoinForUpdate(): Boolean {
        return false
    }

    override fun supportsCurrentTimestampSelection(): Boolean {
        return true
    }

    override fun isCurrentTimestampSelectStringCallable(): Boolean {
        return false
    }

    override fun getCurrentTimestampSelectString(): String {
        return "select current_timestamp"
    }

    private class SQLiteUniqueDelegate(dialect: Dialect) : DefaultUniqueDelegate(dialect) {
        override fun getColumnDefinitionUniquenessFragment(column: Column?): String {
            return " unique"
        }
    }

    private val SQLITE_BUSY = 5
    private val SQLITE_LOCKED = 6
    private val SQLITE_IOERR = 10
    private val SQLITE_CORRUPT = 11
    private val SQLITE_NOTFOUND = 12
    private val SQLITE_FULL = 13
    private val SQLITE_CANTOPEN = 14
    private val SQLITE_PROTOCOL = 15
    private val SQLITE_TOOBIG = 18
    private val SQLITE_CONSTRAINT = 19
    private val SQLITE_MISMATCH = 20
    private val SQLITE_NOTADB = 26

    override fun buildSQLExceptionConversionDelegate(): SQLExceptionConversionDelegate {
        return SQLExceptionConversionDelegate { sqlException, message, sql ->
            val errorCode:Int = JdbcExceptionHelper.extractErrorCode(sqlException) + 0xFF
            if(errorCode == SQLITE_TOOBIG || errorCode == SQLITE_MISMATCH){
                return@SQLExceptionConversionDelegate DataException(message, sqlException, sql)
            } else if (errorCode == SQLITE_BUSY || errorCode == SQLITE_LOCKED) {
                LockAcquisitionException(message, sqlException, sql)
            } else if ((errorCode in SQLITE_IOERR..SQLITE_PROTOCOL) || errorCode == SQLITE_NOTADB) {
                return@SQLExceptionConversionDelegate JDBCConnectionException(message, sqlException, sql)
            }

            null
        }
    }

    object EXTRACTER : TemplatedViolatedConstraintNameExtracter() {
        override fun doExtractConstraintName(sqle: SQLException?): String? {
            val errorCode:Int = JdbcExceptionHelper.extractErrorCode(sqle) + 0xFF

            if (errorCode == SQLiteDialect().SQLITE_CONSTRAINT){
                return extractConstraintName(sqle)
            }
            return null
        }
    }

    override fun getViolatedConstraintNameExtracter(): ViolatedConstraintNameExtracter {
        return EXTRACTER
    }

    override fun supportsUnionAll(): Boolean {
        return true
    }

    override fun canCreateSchema(): Boolean {
        return false
    }

    override fun hasAlterTable(): Boolean {
        return false
    }

    override fun dropConstraints(): Boolean {
        return false
    }

    override fun qualifyIndexName(): Boolean {
        return false
    }

    override fun getAddColumnString(): String {
        return "add column"
    }

    override fun getDropForeignKeyString(): String {
        throw UnsupportedOperationException("No drop foreign key syntax supported by SQLiteDialect")
    }

    override fun getAddForeignKeyConstraintString(constraintName: String?, foreignKey: Array<out String>?, referencedTable: String?, primaryKey: Array<out String>?, referencesPrimaryKey: Boolean): String {
        throw UnsupportedOperationException("No add foreign key syntax supported by SQLiteDialect")
    }

    override fun getAddPrimaryKeyConstraintString(constraintName: String?): String {
        throw UnsupportedOperationException("No add primary key syntax supported by SQLiteDialect")
    }

    override fun supportsCommentOn(): Boolean {
        return true
    }

    override fun supportsIfExistsBeforeTableName(): Boolean {
        return true
    }

    override fun doesRepeatableReadCauseReadersToBlockWriters(): Boolean {
        return true
    }

    override fun getInExpressionCountLimit(): Int {
        return 1000
    }

    override fun getUniqueDelegate(): UniqueDelegate {
        return uniqueDelegate
    }

    override fun getSelectGUIDString(): String {
        return "select hex(randomblob(16))"
    }

    override fun defaultScrollMode(): ScrollMode {
        return ScrollMode.FORWARD_ONLY
    }




}