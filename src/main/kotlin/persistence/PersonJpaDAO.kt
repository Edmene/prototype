package persistence

import javax.persistence.EntityManager
import javax.persistence.Persistence

@Deprecated("This class was been replaced by persistence.GenericDAO")
open class PersonJpaDAO private constructor() {
    private var entityManager:EntityManager? = null

    init {
        entityManager = getEntityManager()
    }

    companion object PersonDAO: PersonJpaDAO()


    internal fun getEntityManager():EntityManager?{
        val factory = Persistence.createEntityManagerFactory("pu-sqlite-jpa")
        if(entityManager == null){
            entityManager = factory.createEntityManager()
        }
        return entityManager
    }

    fun getById(id: Int): Person {
        return entityManager!!.find(Person::class.java, id)
    }

    fun findAll() : MutableList<Any?>{
        return entityManager!!.createQuery("FROM Person")!!.resultList
    }

    fun persist(person: Person) {
        try {
            entityManager!!.transaction.begin()
            entityManager!!.persist(person)
            entityManager!!.transaction.commit()
        } catch (ex: Exception) {
            ex.printStackTrace()
            entityManager!!.transaction.rollback()
        }

    }

    fun merge(person: Person) {
        try {
            entityManager!!.transaction.begin()
            entityManager!!.merge(person)
            entityManager!!.transaction.commit()
        } catch (ex: Exception) {
            ex.printStackTrace()
            entityManager!!.transaction.rollback()
        }

    }

    fun remove(person: Person) {
        var cliente = person
        try {
            entityManager!!.transaction.begin()
            cliente = entityManager!!.find(Person::class.java, person.id)
            entityManager!!.remove(cliente)
            entityManager!!.transaction.commit()
        } catch (ex: Exception) {
            ex.printStackTrace()
            entityManager!!.transaction.rollback()
        }

    }

    fun removeById(id: Int) {
        try {
            val person = getById(id)
            remove(person)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }



}

