package persistence

import javax.persistence.EntityManager
import javax.persistence.Persistence


open class GenericDAO private constructor() {
    var entityManager:EntityManager? = null

    init {
        entityManager = getEntityManager()
    }

    companion object Dao: GenericDAO()

    internal fun getEntityManager():EntityManager?{
        val factory = Persistence.createEntityManagerFactory("sqlite-jpa")
        if(entityManager == null){
            entityManager = factory.createEntityManager()
        }
        return entityManager
    }

    inline fun <reified T: Any, I> getById(id: I): T {
        return entityManager!!.find(T::class.java, id)
    }

    fun findAll(className:String) : MutableList<Any?>{
        return entityManager!!.createQuery("FROM $className")!!.resultList
    }

    fun <T> persist(entity: T) {
        try {
            entityManager!!.transaction.begin()
            entityManager!!.persist(entity)
            entityManager!!.transaction.commit()
        } catch (ex: Exception) {
            ex.printStackTrace()
            entityManager!!.transaction.rollback()
        }

    }

    fun <T> merge(entity: T) {
        try {
            entityManager!!.transaction.begin()
            entityManager!!.merge(entity)
            entityManager!!.transaction.commit()
        } catch (ex: Exception) {
            ex.printStackTrace()
            entityManager!!.transaction.rollback()
        }

    }

    inline fun <reified T, I> remove(id: I) {
        var entityToRemove:T?
        try {
            entityManager!!.transaction.begin()
            entityToRemove = entityManager!!.find(T::class.java, id)
            entityManager!!.remove(entityToRemove)
            entityManager!!.transaction.commit()
        } catch (ex: Exception) {
            ex.printStackTrace()
            entityManager!!.transaction.rollback()
        }

    }



}

