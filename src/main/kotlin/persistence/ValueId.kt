package persistence

import java.io.Serializable
import javax.persistence.*
import java.util.Date

@Embeddable
data class ValueId(

        /*
        @Id @OneToMany(targetEntity = persistence.Person::class)
        var personList: List<persistence.Person>,
        */

        @ManyToOne(targetEntity = Person::class,optional=false)
        @JoinColumn(name="person",referencedColumnName="id")
        var person: Person,

        @ManyToOne(targetEntity = ValueList::class, optional = false)
        @JoinColumn(name = "valueList", referencedColumnName = "id")
        var valueList: ValueList,

        var date: Date

        /*
        var id: Int,
        var person: persistence.Person,
        var valueList: persistence.ValueList
        */
) : Serializable

